# Domain Intel Service

A simple API which can receive a domain and query multiple data sources to gather information about the domain and any
associated hosts and IP addresses.

The service is a serverless AWS application, developed and deployed using the
[Serverless framework](https://www.serverless.com/).

## Data Sources

_Note: For simplicity due to time constraints, currently only the DNS lookup service is used._

- DNS lookup
    - Queries A and CNAME DNS records to find IP addresses linked to the domain.
- PhishTank _(not implemented)_
    - Uses the [PhishTank](https://phishtank.org/) API to assess whether the domain is associated with a phishing scam.
- Censys _(not implemented)_
    - Uses the [Censys](https://search.censys.io/api) API to find certificates registered for the domain and subdomains.

## Local setup

### Dependencies

Ensure the following are installed before building a development environment:

- [Poetry](https://python-poetry.org/)
- [NPM](https://www.npmjs.com/)

### Development environment

To set up the Python and JavaScript packages required for local development and deployment:

```shell
cp .env.example .env
make build
````

To run tests and linting:

```shell
make test
make lint
```

## Deployment

```shell
make deploy
```

# Usage

After running the Serverless deploy command, a URL for the handler will be presented.

Go to `{api_gateway_url}/domain?domain=google.com` to get query results.
