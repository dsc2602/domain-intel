from lambda_http.responses import success, bad_request, server_error


def test_success():
    body = {"result": "success!"}

    response = success(body)

    assert response["statusCode"] == 200
    assert response["body"] == "{\"result\": \"success!\"}"


def test_bad_request_with_error():
    response = bad_request("missing domain value")

    assert response["statusCode"] == 400
    assert response["body"] == "{\"error\": \"missing domain value\"}"


def test_bad_request_without_error():
    response = bad_request()

    assert response["statusCode"] == 400
    assert "body" not in response


def test_server_error_with_error():
    response = server_error("something went wrong")

    assert response["statusCode"] == 500
    assert response["body"] == "{\"error\": \"something went wrong\"}"


def test_server_error_without_error():
    response = bad_request()

    assert response["statusCode"] == 400
    assert "body" not in response
