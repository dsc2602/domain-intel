import unittest
from unittest.mock import patch

import dns
from aws_lambda_powertools.utilities.data_classes import APIGatewayProxyEvent
from aws_lambda_powertools.utilities.typing import LambdaContext

from lambda_http.handler import get_domain


class HandlerTest(unittest.TestCase):
    @patch("dns.resolver.resolve")
    def test_handler_success(self, dns_patch):
        dns_response = [
            dns.rdtypes.IN.A.A(rdclass=dns.rdataclass.IN, rdtype=dns.rdatatype.A, address="127.0.0.1")
        ]

        dns_patch.return_value = dns_response
        event = APIGatewayProxyEvent(data={
            "queryStringParameters": {
                "domain": "example.uk",
            }
        })

        context = LambdaContext()

        response = get_domain(event, context)

        assert response["statusCode"] == 200
        assert response["body"] == "{\"domain\": \"example.uk\", \"data\": [{\"A\": [\"127.0.0.1\"], \"CNAME\": [" \
                                   "\"127.0.0.1\"]}], \"known_ips\": [\"127.0.0.1\"]}"

    def test_handler_without_query_parameters(self):
        event = APIGatewayProxyEvent(data={})

        context = LambdaContext()

        response = get_domain(event, context)

        assert response["statusCode"] == 400
        assert response["body"] == "{\"error\": \"'domain' query parameter is required\"}"

    def test_handler_with_query_parameters_without_value(self):
        event = APIGatewayProxyEvent(data={
            "queryStringParameters": {
                "domain": "",
            }
        })

        context = LambdaContext()

        response = get_domain(event, context)

        assert response["statusCode"] == 400
        assert response["body"] == "{\"error\": \"'domain' query parameter is required\"}"

    def test_handler_with_query_parameters_with_none_value(self):
        event = APIGatewayProxyEvent(data={
            "queryStringParameters": {
                "domain": None,
            }
        })

        context = LambdaContext()

        response = get_domain(event, context)

        assert response["statusCode"] == 400
        assert response["body"] == "{\"error\": \"'domain' query parameter is required\"}"

    def test_handler_with_empty_query_parameters(self):
        event = APIGatewayProxyEvent(data={
            "queryStringParameters": {},
        })

        context = LambdaContext()

        response = get_domain(event, context)

        assert response["statusCode"] == 400
        assert response["body"] == "{\"error\": \"'domain' query parameter is required\"}"
