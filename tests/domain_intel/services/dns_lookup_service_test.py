import unittest
from unittest.mock import patch

import dns.rdtypes.IN.A

from domain_intel.services.dns_lookup_service import DnsLookupService


class DnsLookupServiceTest(unittest.TestCase):
    @patch("dns.resolver.resolve")
    def test_get_domain_info(self, dns_patch):
        dns_response = [
            dns.rdtypes.IN.A.A(rdclass=dns.rdataclass.IN, rdtype=dns.rdatatype.A, address="127.0.0.1")
        ]

        dns_patch.return_value = dns_response

        service = DnsLookupService()
        domain = "google.com"

        domain_info = service.get_domain_info(domain)

        assert domain_info.domain == domain
        assert domain_info.data == [{
            "A": ["127.0.0.1"],
            "CNAME": ["127.0.0.1"],
        }]
        assert domain_info.known_ips == ["127.0.0.1"]

    @patch("dns.resolver.resolve")
    def test_get_domain_info_no_answer(self, dns_patch):
        dns_patch.side_effect = dns.resolver.NoAnswer()

        service = DnsLookupService()
        domain = "google.com"

        domain_info = service.get_domain_info(domain)

        assert domain_info.domain == domain
        assert domain_info.data == [{}]
        assert domain_info.known_ips == []

    @patch("dns.resolver.resolve")
    def test_get_domain_info_no_nameservers(self, dns_patch):
        dns_patch.side_effect = dns.resolver.NoNameservers()

        service = DnsLookupService()
        domain = "google.com"

        domain_info = service.get_domain_info(domain)

        assert domain_info.domain == domain
        assert domain_info.data == [{}]
        assert domain_info.known_ips == []
