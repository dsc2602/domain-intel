import unittest
from unittest.mock import patch

import dns

from domain_intel.domain_query_runner import get_domain_info
from domain_intel.models.domain import Domain


class DomainQueryRunnerTest(unittest.TestCase):
    @patch("dns.resolver.resolve")
    def test_get_domain_info(self, dns_patch):
        dns_response = [
            dns.rdtypes.IN.A.A(rdclass=dns.rdataclass.IN, rdtype=dns.rdatatype.A, address="127.0.0.1")
        ]

        dns_patch.return_value = dns_response

        domain = "example.uk"
        domain_info = get_domain_info(domain)

        assert isinstance(domain_info, Domain)
        assert domain_info.domain == domain
        assert domain_info.data == [{
            "A": ["127.0.0.1"],
            "CNAME": ["127.0.0.1"],
        }]
        assert domain_info.known_ips == ["127.0.0.1"]
