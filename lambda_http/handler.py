from aws_lambda_powertools.utilities.data_classes import APIGatewayProxyEvent, event_source
from aws_lambda_powertools.utilities.typing import LambdaContext

from domain_intel.domain_query_runner import get_domain_info
from lambda_http.responses import success, bad_request


@event_source(data_class=APIGatewayProxyEvent)
def get_domain(event: APIGatewayProxyEvent, context: LambdaContext):
    try:
        domain = event.query_string_parameters["domain"]
        if domain is None or domain == "":
            raise KeyError
    except (TypeError, KeyError):
        return bad_request("'domain' query parameter is required")

    domain_info = get_domain_info(domain)

    return success(domain_info.__dict__)
