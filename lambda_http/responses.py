import json
from http import HTTPStatus
from typing import TypedDict, Optional


class Response(TypedDict):
    statusCode: int
    body: Optional[str]


def _response(status_code: int, body: Optional[dict] = None) -> Response:
    response_data = {"statusCode": status_code}

    if body is not None:
        response_data["body"] = json.dumps(body)

    return response_data


def _error_response(status_code: int, error_text: Optional[str] = None) -> Response:
    if error_text is not None:
        return _response(status_code, {"error": error_text, })

    return _response(status_code)


def success(body: dict) -> Response:
    return _response(HTTPStatus.OK, body)


def bad_request(error_text: Optional[str] = None) -> Response:
    return _error_response(HTTPStatus.BAD_REQUEST, error_text)


def server_error(error_text: str) -> Response:
    return _error_response(HTTPStatus.INTERNAL_SERVER_ERROR, error_text)
