include .env
export

build:
	npm ci
	poetry install

lint:
	poetry run pylint domain_intel

test:
	poetry run pytest tests

deploy:
	serverless deploy --stage=${STAGE} --region=${REGION}

destroy:
	serverless remove --stage=${STAGE} --region=${REGION}