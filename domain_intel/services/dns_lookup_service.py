from typing import List

from dns import rdatatype, resolver

from domain_intel.services.base_domain_intel_service import BaseDomainIntelService


class DnsLookupService(BaseDomainIntelService):
    record_types: List[str] = [
        rdatatype.A,
        rdatatype.CNAME,
    ]

    def _get_domain_data(self, domain: str) -> dict:
        domain_data = {}

        for record_type in self.record_types:
            try:
                answers = resolver.resolve(domain, record_type)
                domain_data[record_type.name] = [answer.address for answer in answers]
            except (resolver.NoAnswer, resolver.NoNameservers):
                continue

        return domain_data

    def _extract_known_ips(self, data: dict) -> List[str]:
        addresses = set()

        for records in data.values():
            for record in records:
                addresses.add(record)

        return list(addresses)
