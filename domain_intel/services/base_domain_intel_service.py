from abc import ABC, abstractmethod
from typing import List

from domain_intel.models.domain import Domain


class BaseDomainIntelService(ABC):
    @abstractmethod
    def _get_domain_data(self, domain: str) -> dict:
        ...

    def get_domain_info(self, domain: str) -> Domain:
        data = self._get_domain_data(domain)
        return self._make_domain_result_model(domain, data)

    @abstractmethod
    def _extract_known_ips(self, data: dict) -> List[str]:
        ...

    def _make_domain_result_model(self, domain: str, data: dict) -> Domain:
        domain_model = Domain()

        domain_model.domain = domain
        domain_model.data = [data]
        domain_model.known_ips = self._extract_known_ips(data)

        return domain_model
