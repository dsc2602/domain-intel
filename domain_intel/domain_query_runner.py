from functools import reduce
from typing import List, Type

from domain_intel.models.domain import Domain
from domain_intel.services.base_domain_intel_service import BaseDomainIntelService
from domain_intel.services.dns_lookup_service import DnsLookupService


def get_domain_info(domain: str):
    return DomainQueryRunner(domain).run_query()


class DomainQueryRunner:
    domain: str

    services: List[Type[BaseDomainIntelService]] = [
        DnsLookupService,
    ]

    def __init__(self, domain: str):
        self.domain = domain

    def run_query(self) -> Domain:
        domain_models = []
        for service_class in self.services:
            service = service_class()
            domain_models.append(service.get_domain_info(self.domain))

        def reducer(domain_a: Domain, domain_b: Domain) -> Domain:
            domain_a.data += domain_b.data
            return domain_a

        return reduce(reducer, domain_models)
