from typing import List


class Domain:
    domain: str
    known_ips: List[str]
    data: List[dict]
